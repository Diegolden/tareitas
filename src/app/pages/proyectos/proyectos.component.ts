import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { UserServicesService } from 'src/app/services/user-services.service';
import { proyecto } from 'src/app/models/proyectos.interface';
import { Router } from '@angular/router';


/**
 * @title Table with pagination
 */

@Component({
  selector: 'app-proyectos',
  styleUrls: ['./proyectos.component.css'],
  templateUrl: './proyectos.component.html',
})
export class ProyectosComponent implements AfterViewInit, OnInit {
  proyec: any[];
  displayedColumns: string[] = ['id', 'nombre', 'publicacion', 'recompensa', 'opciones', 'ir'];
  dataSource = new MatTableDataSource<proyecto>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private pro: UserServicesService,
    private rutas: Router ){
    this.getproyectos();
  }

  getproyectos(){
    this.pro.getProyectos().
    subscribe( (proyectos: any) => {
      console.log(' respuesta de proyectos, ', proyectos);
      this.proyec = proyectos.data;
    })
  }

  ngOnInit(){
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  verProyecto(i:number){
     this.rutas.navigate(['/proyecto',i]);
   
  }
}

const ELEMENT_DATA: proyecto[] = [];




